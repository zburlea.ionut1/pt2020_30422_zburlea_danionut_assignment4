package bussinessLayer;

public class BaseProduct implements MenuItem {
	private static final long serialVersionUID = 1L;
	private String name;
	private float price;
	public BaseProduct(String name, float price) {
		this.name = name;
		this.price = price;
	}
	public float computePrice() {
		return this.price;
	}
	public String getName() {
		return this.name;
	}
	@Override
	public String toString() {
		return "BaseProduct [name=" + name + ", price=" + price + "]";
	}
	
	
}
