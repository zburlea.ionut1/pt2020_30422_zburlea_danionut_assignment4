package bussinessLayer;

import java.util.ArrayList;

public interface MenuItem {
	public float computePrice();
	public String getName();
}
