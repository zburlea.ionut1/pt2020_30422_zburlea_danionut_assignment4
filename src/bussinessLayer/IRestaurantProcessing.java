package bussinessLayer;

import java.io.Serializable;
import java.util.ArrayList;

public interface IRestaurantProcessing extends Serializable{
	public MenuItem createMenuItem(String name, float price);
	public void editMenuItem(String name, float price);
	public void addMenuItem(MenuItem menuItem);
	public void deleteMenuItem(String name);
	public Order createOrder(int orderID, String date, int tableNr, ArrayList<MenuItem> orderItems);
	public float orderTotalPrice(Order order);
	public void writeBill(Order order);
}
