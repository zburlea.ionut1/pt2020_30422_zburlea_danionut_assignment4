package presentationLayer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import bussinessLayer.BaseProduct;
import bussinessLayer.Restaurant;

import java.awt.EventQueue;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CreateMenuItem extends JFrame{
	private JTextField textField;
	private JTextField textField_1;
	private JPanel contentPane;
	private JButton btnGoBack;

	public CreateMenuItem(Restaurant restaurant) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 418, 261);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setLayout(null);
		
		JLabel lblInsertName = new JLabel("Insert name:");
		lblInsertName.setBounds(33, 37, 90, 26);
		getContentPane().add(lblInsertName);
		
		textField = new JTextField();
		textField.setBounds(135, 39, 116, 22);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblInsertPrice = new JLabel("Insert price:");
		lblInsertPrice.setBounds(33, 84, 90, 16);
		getContentPane().add(lblInsertPrice);
		
		textField_1 = new JTextField();
		textField_1.setBounds(135, 91, 116, 22);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnAddNewMenu = new JButton("Add new menu item");
		btnAddNewMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = textField.getText();
				String priceString = textField_1.getText();
				float price = Float.parseFloat(priceString);
				restaurant.createMenuItem(name, price);
				BaseProduct baseProduct= new BaseProduct(name, price);
				restaurant.addMenuItem(baseProduct);
			}
		});
		btnAddNewMenu.setBounds(135, 126, 147, 22);
		getContentPane().add(btnAddNewMenu);
		
		btnGoBack = new JButton("go back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdministratorGraphicalUserInterface administratorGraphicalUserInterface = new AdministratorGraphicalUserInterface(restaurant);
				administratorGraphicalUserInterface.setVisible(true);
				setVisible(false);
			}
		});
		btnGoBack.setBounds(12, 125, 97, 25);
		contentPane.add(btnGoBack);
	}

}
