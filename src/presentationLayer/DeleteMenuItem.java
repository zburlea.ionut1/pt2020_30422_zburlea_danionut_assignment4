package presentationLayer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import bussinessLayer.Restaurant;

import java.awt.EventQueue;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DeleteMenuItem extends JFrame{
	private JTextField textField;
	private JPanel contentPane;
	private JButton btnGoBack;

	public DeleteMenuItem(Restaurant restaurant) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 220);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setLayout(null);
		
		JLabel lblInsertName = new JLabel("Insert name:");
		lblInsertName.setBounds(22, 32, 92, 21);
		getContentPane().add(lblInsertName);
		
		textField = new JTextField();
		textField.setBounds(126, 31, 116, 22);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnDeleteMenuItem = new JButton("Delete menu item");
		btnDeleteMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = textField.getText();
				restaurant.deleteMenuItem(name);
			}
		});
		btnDeleteMenuItem.setBounds(126, 83, 133, 22);
		getContentPane().add(btnDeleteMenuItem);
		
		btnGoBack = new JButton("go back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdministratorGraphicalUserInterface administratorGraphicalUserInterface = new AdministratorGraphicalUserInterface(restaurant);
				administratorGraphicalUserInterface.setVisible(true);
				setVisible(false);
			}
		});
		btnGoBack.setBounds(12, 82, 97, 25);
		contentPane.add(btnGoBack);
	}
	
}
