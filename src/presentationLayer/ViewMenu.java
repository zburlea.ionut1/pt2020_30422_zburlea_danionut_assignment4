package presentationLayer;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import bussinessLayer.MenuItem;
import bussinessLayer.Restaurant;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

public class ViewMenu extends JFrame{
	private JPanel contentPane;
	private JTable table;
	private Object[] columsName;
	private Object[] rowData = new Object[2];
	JPanel panel = new JPanel();
	public ViewMenu(Restaurant restaurant) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 690, 445);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setLayout(null);
		
		panel.setBounds(94, 69, 528, 316);
		panel.setLayout(new BorderLayout());
	

		ArrayList<MenuItem> menu = restaurant.getMenu();
		
		 String[] colName = new String[] { "Product Name" ,"Price" };
		 Object[][] products = new Object[menu.size()][2];
		 
		 
		for(int row =0; row<menu.size(); row++) {
			products[row][0] = menu.get(row).getName();
			products[row][1] = menu.get(row).computePrice();
		}
		
		JTable table = new JTable(products, colName);
		
		panel.add(new JScrollPane(table), BorderLayout.CENTER);
		
		JButton btnGoBack = new JButton("Go back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdministratorGraphicalUserInterface administratorGraphicalUserInterface = new AdministratorGraphicalUserInterface(restaurant);
				administratorGraphicalUserInterface.setVisible(true);
				setVisible(false);
			}
		});
		btnGoBack.setBounds(0, 0, 97, 25);
		contentPane.add(btnGoBack);
		

		contentPane.add(panel);
	}
}
