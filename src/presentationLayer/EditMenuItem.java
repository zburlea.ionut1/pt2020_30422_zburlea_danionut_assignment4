package presentationLayer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import bussinessLayer.Restaurant;

import java.awt.EventQueue;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EditMenuItem extends JFrame{
	private JTextField textField;
	private JTextField textField_1;
	private JPanel contentPane;
	private JButton btnGoBack;
	
	public EditMenuItem(Restaurant restaurant) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 220);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setLayout(null);
		
		JLabel lblInsertName = new JLabel("Insert name:");
		lblInsertName.setBounds(12, 25, 93, 27);
		getContentPane().add(lblInsertName);
		
		textField = new JTextField();
		textField.setBounds(117, 27, 116, 22);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblInsertPrice = new JLabel("Insert price:");
		lblInsertPrice.setBounds(12, 65, 78, 16);
		getContentPane().add(lblInsertPrice);
		
		textField_1 = new JTextField();
		textField_1.setBounds(117, 62, 116, 22);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnEditMenuItem = new JButton("Edit menu item");
		btnEditMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = textField.getText();
				String priceString = textField_1.getText();
				float price = Float.parseFloat(priceString);
				restaurant.editMenuItem(name, price);
			}
		});
		btnEditMenuItem.setBounds(117, 97, 132, 25);
		getContentPane().add(btnEditMenuItem);
		
		btnGoBack = new JButton("go back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdministratorGraphicalUserInterface administratorGraphicalUserInterface = new AdministratorGraphicalUserInterface(restaurant);
				administratorGraphicalUserInterface.setVisible(true);
				setVisible(false);
			}
		});
		btnGoBack.setBounds(0, 97, 97, 25);
		contentPane.add(btnGoBack);
	}

}
