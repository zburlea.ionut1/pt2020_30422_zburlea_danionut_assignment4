package presentationLayer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import bussinessLayer.MenuItem;
import bussinessLayer.Restaurant;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class SpecialOffer extends JFrame{
	private JTextField textField;
	private JTextField textField_1;
	private JPanel contentPane;
	public SpecialOffer(Restaurant restaurant) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 429, 265);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setLayout(null);
		
		JLabel lblInsertName = new JLabel("Insert name:");
		lblInsertName.setBounds(12, 28, 83, 33);
		getContentPane().add(lblInsertName);
		
		textField = new JTextField();
		textField.setBounds(153, 33, 116, 22);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblInsertMenuItems = new JLabel("Insert menu items:");
		lblInsertMenuItems.setBounds(12, 74, 127, 33);
		getContentPane().add(lblInsertMenuItems);
		
		textField_1 = new JTextField();
		textField_1.setBounds(153, 79, 126, 22);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnGoBack = new JButton("go back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdministratorGraphicalUserInterface administratorGraphicalUserInterface = new AdministratorGraphicalUserInterface(restaurant);
				administratorGraphicalUserInterface.setVisible(true);
				setVisible(false);
			}
		});
		btnGoBack.setBounds(12, 131, 97, 25);
		getContentPane().add(btnGoBack);
		
		JButton btnCreateSpecialOffer = new JButton("create special offer");
		btnCreateSpecialOffer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String specialofferName = textField.getText();
				ArrayList<MenuItem> specialOfferItems = new ArrayList<MenuItem>();
				String specialOfferItemsString = textField_1.getText();
				String[] specialOfferItemsStrings = specialOfferItemsString.split(",");
				for(String orderItem : specialOfferItemsStrings) {
					for(MenuItem menuItem : restaurant.getMenu()) {
						if(orderItem.equals(menuItem.getName())) {
							specialOfferItems.add(menuItem);
						}
					}
				}
				restaurant.createSpecialOffer(specialofferName, specialOfferItems);
			}
		});
		btnCreateSpecialOffer.setBounds(153, 131, 177, 25);
		getContentPane().add(btnCreateSpecialOffer);
	}
}
