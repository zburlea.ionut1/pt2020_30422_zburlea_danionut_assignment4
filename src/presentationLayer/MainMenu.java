package presentationLayer;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bussinessLayer.Restaurant;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class MainMenu extends JFrame {

	private static Restaurant restaurant;
	private JPanel contentPane;
	/**
	 * Launch the application.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, IOException {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					restaurant = new Restaurant();
					MainMenu frame = new MainMenu(restaurant);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainMenu(Restaurant restaurant) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 690, 445);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPleaseSelectWho = new JLabel("Please select who you are:");
		lblPleaseSelectWho.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblPleaseSelectWho.setBounds(188, 26, 276, 38);
		contentPane.add(lblPleaseSelectWho);
		
		JButton btnAdministrator = new JButton("Administrator");
		btnAdministrator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AdministratorGraphicalUserInterface administratorGraphicalUserInterface = new AdministratorGraphicalUserInterface(restaurant);
				administratorGraphicalUserInterface.setVisible(true);
				setVisible(false);
			}
		});
		btnAdministrator.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnAdministrator.setBounds(244, 99, 172, 59);
		contentPane.add(btnAdministrator);
		
		JButton btnWaiter = new JButton("Waiter");
		btnWaiter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				WaiterGraphicalUserInterface waiterGraphicalUserInterface = new WaiterGraphicalUserInterface(restaurant);
				waiterGraphicalUserInterface.setVisible(true);
				setVisible(false);
			}
		});
		btnWaiter.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnWaiter.setBounds(244, 200, 172, 59);
		contentPane.add(btnWaiter);
		
		JButton btnChef = new JButton("Chef");
		btnChef.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ChefGraphicalUserInterface chefGraphicalUserInterface= new ChefGraphicalUserInterface(restaurant);
				chefGraphicalUserInterface.setVisible(true);
				setVisible(false);
			}
		});
		btnChef.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnChef.setBounds(244, 303, 172, 59);
		contentPane.add(btnChef);
	}
}
