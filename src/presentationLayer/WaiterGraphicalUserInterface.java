package presentationLayer;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bussinessLayer.Restaurant;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;

public class WaiterGraphicalUserInterface extends JFrame {

	private JPanel contentPane;

	public WaiterGraphicalUserInterface(Restaurant restaurant) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 466, 485);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblWelcomeWaiter = new JLabel("Welcome, waiter!");
		lblWelcomeWaiter.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblWelcomeWaiter.setBounds(144, 13, 185, 34);
		contentPane.add(lblWelcomeWaiter);
		
		JLabel lblPleaseSelectWhat = new JLabel("Please select what action you want to perform:");
		lblPleaseSelectWhat.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPleaseSelectWhat.setBounds(28, 60, 377, 34);
		contentPane.add(lblPleaseSelectWhat);
		
		JButton btnCreateAnOrder = new JButton("Create an order");
		btnCreateAnOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CreateOrder createOrder = new CreateOrder(restaurant);
				createOrder.setVisible(true);
				setVisible(false);
			}
		});
		btnCreateAnOrder.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnCreateAnOrder.setBounds(28, 118, 301, 62);
		contentPane.add(btnCreateAnOrder);
		
		JButton btnNewButton = new JButton("Compute price for an order");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ComputePrice computePrice = new ComputePrice(restaurant);
				computePrice.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton.setBounds(28, 193, 301, 62);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Generate bill");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GenerateBill generateBill = new GenerateBill(restaurant);
				generateBill.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton_1.setBounds(28, 268, 301, 62);
		contentPane.add(btnNewButton_1);
		
		JButton btnGoBack = new JButton("Go back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainMenu mainMenu = new MainMenu(restaurant);
				mainMenu.setVisible(true);
				setVisible(false);
			}
		});
		btnGoBack.setBounds(35, 21, 97, 25);
		contentPane.add(btnGoBack);
		
		JButton btnViewOrders = new JButton("View orders");
		btnViewOrders.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewOrders viewOrders = new ViewOrders(restaurant);
				viewOrders.setVisible(true);
				setVisible(false);
			}
		});
		btnViewOrders.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnViewOrders.setBounds(28, 343, 301, 62);
		contentPane.add(btnViewOrders);
	}
}
